# Sort
Una programa que deja ejecutar varios algoritmos de ordenamiento con el próposito de de comprobar la diferencia en rendamiento de los algoritmos.
[sort](https://git.cromer.cl/UBB/sort)

## Autores
Christopher Cromer

Rodolfo Cuevas

## Compilar
Hay tres blancos disponible para usar con GNU Make:

	make sort
Compilar el programa

	make test
Comprobar que los algoritmos y programa corren como esperado

	make informe
Compilar el informe si pdflatex está disponible en el sistema

## Correr el programa
	./sort
