/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * Unir los resultados de "dividir y conquistar" de merge sort
 * @param array El array a ordenar
 * @param temp Un array temporario para trabajar
 * @param prev_left El lado izquerda anterior
 * @param prev_middle La particion de medio anterior
 * @param right El lado derecha
 */
void merge(int *array, int *temp, int prev_left, int prev_middle, int right) {
	int i = 0;
	int left = prev_left;
	int middle = prev_middle - 1;
	int far_right = right - left + 1;

	while (prev_left <= middle && prev_middle <= right) {
		if (array[prev_left] < array[prev_middle]) {
			temp[i++] = array[prev_left++];
		}
		else {
			temp[i++] = array[prev_middle++];
		}
	}

	while (prev_left <= middle) {
		temp[i++] = array[prev_left++];
	}

	while (prev_middle <= right) {
		temp[i++] = array[prev_middle++];
	}

	for (i = 0; i < far_right; i++) {
		array[left + i] = temp[i];
	}
}

/**
 * Correr el merge sort recursivamente
 * @param array El array a ordenar
 * @param temp Un array temporario para trabajar
 * @param left El lado izquerda a ordenar
 * @param right El lado derercha a ordenar
 */
void merge_sort_run(int *array, int *temp, int left, int right) {
	if (left != right) {
		int middle = (left + right) / 2;
		merge_sort_run(array, temp, left, middle);
		merge_sort_run(array, temp, middle + 1, right);
		merge(array, temp, left, middle + 1, right);
	}
}

/**
 * Usar el algoritmo de merge sort
 * @param array El array a ordenar
 * @param n El tamaño del array
 */
void merge_sort(int *array, int n) {
	int *temp = malloc(sizeof(int)* n);
	if (temp == NULL) {
		fprintf(stderr, "Error: Out of heap space!\n");
		exit(5);
	}
	merge_sort_run(array, temp, 0, n - 1);
	free(temp);
}
