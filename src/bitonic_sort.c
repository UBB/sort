/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Verificar si x es de 2^n
 * @param x El valor a verificar
 * @return Retorna 1 si es de 2^n ó 0 al contrario;
 */
int power_of_two(int n) {
	if (n == 0) {
		return 0;
	}

	while (n != 1) {
		if (n % 2 != 0) {
			return 0;
		}
		n = n / 2;
	}
	return 1;
}

/**
 * Buscar un valor que es de potencia de 2 pero menor que n
 * @param n El valor de n
 * @return Un potencia de 2 que es menor que n
 */
int greatest_power_of_two_less_than(int n) {
	int k = 1;
	while (k > 0 && k < n) {
		k = (int) ((unsigned int) k << 1);
	}
	k = (int) ((unsigned int) k >> 1);
	return (int) k;
}

/**
 * Comparar y intercambiar los valores para darle orden
 * @param i El primer indice a comparar
 * @param j El segundo indice a comparar
 * @param dir La dirección a ordenar, 1 para ascendentemente o 0 por descendentamente
 * @param array El array a ordenar
 */
void compare(int i, int j, int dir, int *array) {
	int temp;
	if (dir == (array[i] > array[j])){
		temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}

/**
 * Unir la secuencia
 * @param low El parte inferior
 * @param c El parte superior n
 * @param dir La dirección a ordenar, 1 para ascendentemente o 0 por descendentamente
 * @param array El array a ordenar
 */
void bitonicmerge(int low, int n, int dir, int *array) {
	int i;
	int k;

	if (n > 1) {
		k = n / 2;
		for (i = low; i < low + k; i++){
			compare(i, i + k, dir, array);
		}
		bitonicmerge(low, k, dir, array);
		bitonicmerge(low + k, k, dir, array);
	}
}

/**
 * Unir la secuencia cuando n no es de potencia 2
 * @param low El parte inferior
 * @param c El parte superior n
 * @param dir La dirección a ordenar, 1 para ascendentemente o 0 por descendentamente
 * @param array El array a ordenar
 */
void bitonicmerge_no2(int low, int n, int dir, int *array) {
	int i;
	int k;

	if (n > 1) {
		k = greatest_power_of_two_less_than(n);
		for (i = low; i < low + n-k; i++){
			compare(i, i + k, dir, array);
		}
		bitonicmerge_no2(low, k, dir, array);
		bitonicmerge_no2(low + k, n - k, dir, array);
	}
}

/**
 * Generar la secuencia bitonica en forma de piramide
 * @param low El parte inferior
 * @param c El parte superior n
 * @param dir La dirección a ordenar, 1 para ascendentemente o 0 por descendentamente
 * @param array El array a ordenar
 */
void recbitonic(int low, int n, int dir, int *array) {
	int k;

	if (n > 1) {
		k = n / 2;
		recbitonic(low, k, 1, array);
		recbitonic(low + k, k, 0, array);
		bitonicmerge(low, n, dir, array);
	}
}

/**
 * Generar la secuencia bitonica en forma de piramide cuando n no es de potencia 2
 * @param low El parte inferior
 * @param c El parte superior n
 * @param dir La dirección a ordenar, 1 para ascendentemente o 0 por descendentamente
 * @param array El array a ordenar
 */
void recbitonic_no2(int low, int n, int dir, int *array) {
	int k;

	if (n > 1){
		k = n / 2;
		recbitonic_no2(low, k, !dir, array);
		recbitonic_no2(low + k, n - k, dir, array);
		bitonicmerge_no2(low, n, dir, array);
	}
}

/**
 * Ordenar el arreglo completo
 * @param array El array a ordenar
 * @param n El tamaño del array
 * @param dir La dirección a ordenar, 1 para ascendentemente o 0 por descendentamente
 */
void sort(int *array, int n, int dir) {
	if (power_of_two(n)) {
		recbitonic(0, n, dir, array);
	}
	else {
		recbitonic_no2(0, n, dir, array);
	}
}

/**
 * Usar el algoritmo de bitonic sort
 * @param array El array a ordenar
 * @param n El tamaño del array
 */
void bitonic_sort(int *array, int n) {
	sort(array, n, 1);
}
