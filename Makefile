CC=gcc
CFLAGS=-Wall -Isrc/include -DDEBUG -g
LDFLAGS=-lm
SRC=src/sort.c src/random.c src/bubble_sort.c src/timer.c src/counting_sort.c src/quick_sort.c src/merge_sort.c src/bitonic_sort.c src/selection_sort.c
OBJ=$(SRC:.c=.o)

all: sort informe

sort: $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

informe:
# if pdflatex is installed create the informe
ifneq (, $(shell which pdflatex))
	make -C doc
	mv doc/Informe.pdf Informe.pdf
endif

test:
	make -C test
	test/test

clean: cleansort cleaninforme cleantest

cleansort:
	rm -f src/*.o sort

cleaninforme:
	make -C doc clean
	rm -f Informe.pdf

cleantest:
	make -C test clean

.PHONY: all sort informe test clean cleansort cleaninforme cleantest
