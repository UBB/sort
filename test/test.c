/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "random.h"
#include "bubble_sort.h"
#include "counting_sort.h"
#include "selection_sort.h"
#include "bitonic_sort.h"
#include "merge_sort.h"
#include "quick_sort.h"

static int *test_case;
static int *test_array;
static int *qarray;

/**
 * Este función va a comparar valores para devolver los valores en orden ascendida
 * @param a El primer valor a comparar
 * @param b El segundo valor a comparar
 * @return Retorna la comparación de los dos argumentos
 */
int compar(const void *a, const void *b) {
	return (*(int*) a - *(int*) b);
}

/**
 * Limpiar la memoria al salir
 */
void cleanup() {
	free(test_case);
	free(test_array);
	free(qarray);
}

/**
 * El programa de test
 */
int main(int argc, char **argv) {
	int n = 50000;
	int n2 = 32768;
	int i;
	int gen;
	int pass;
	int passed = 0;
	int failed = 0;

	test_case = malloc(sizeof(int) * n);
	if (test_case == NULL) {
		fprintf(stderr, "Error: Out of heap space!\n");
		exit(1);
	}
	qarray = malloc(sizeof(int) * n);
	if (qarray == NULL) {
		fprintf(stderr, "Error: Out of heap space!\n");
		exit(1);
	}
	test_array = malloc(sizeof(int) * n);
	if (test_array == NULL) {
		fprintf(stderr, "Error: Out of heap space!\n");
		exit(1);
	}

	atexit(cleanup);

	fprintf(stdout, "Running tests:\n");

	// Test random number generation
	pass = 1;
	fprintf(stdout, "\trandom generation: ");
	fflush(stdout);
	for (i = 0; i < 5000000; i++) {
		gen = gen_rand(-1000, 1000);
		if ((gen < -1000 || gen > 1000) && pass) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Prepare for sort tests
	for (i = 0; i < n; i++) {
		test_case[i] = gen_rand(-1000000, 1000000);
	}
	memcpy(qarray, test_case, sizeof(int) * n);
	qsort(qarray, n, sizeof(int), compar);

	// Test bubble sort
	pass = 1;
	memcpy(test_array, test_case, sizeof(int) * n);
	fprintf(stdout, "\tbubble sort: ");
	fflush(stdout);
	bubble_sort(test_array, n);
	for (i = 0; i < n; i++) {
		if (test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Test counting sort
	pass = 1;
	memcpy(test_array, test_case, sizeof(int) * n);
	fprintf(stdout, "\tcounting sort: ");
	fflush(stdout);
	counting_sort(test_array, n);
	for (i = 0; i < n; i++) {
		if (test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Test quick sort
	pass = 1;
	memcpy(test_array, test_case, sizeof(int) * n);
	fprintf(stdout, "\tquick sort: ");
	fflush(stdout);
	quick_sort(test_array, n);
	for (i = 0; i < n; i++) {
		if (test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Test selection sort
	pass = 1;
	memcpy(test_array, test_case, sizeof(int) * n);
	fprintf(stdout, "\tselection sort: ");
	fflush(stdout);
	selection_sort(test_array, n);
	for (i = 0; i < n; i++) {
		if (test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Test merge sort
	pass = 1;
	memcpy(test_array, test_case, sizeof(int) * n);
	fprintf(stdout, "\tmerge sort: ");
	fflush(stdout);
	merge_sort(test_array, n);
	for (i = 0; i < n; i++) {
		if (test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Test bitonic sort without 2^n
	pass = 1;
	memcpy(test_array, test_case, sizeof(int) * n);
	fprintf(stdout, "\tbitonic sort: ");
	fflush(stdout);
	bitonic_sort(test_array, n);
	for (i = 0; i < n; i++) {
		if (test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}

	// Test bitonic sort with 2^n
	pass = 1;
	int *bitonic_test_array = malloc(sizeof(int) * n2);
	if (bitonic_test_array == NULL) {
		fprintf(stderr, "Error: Out of heap space!\n");
		exit(1);
	}
	for (i = 0; i < n2; i++) {
		bitonic_test_array[i] = gen_rand(-1000000, 1000000);
	}
	memcpy(qarray, bitonic_test_array, sizeof(int) * n2);
	qsort(qarray, n2, sizeof(int), compar);
	fprintf(stdout, "\tbitonic sort 2^n: ");
	fflush(stdout);
	bitonic_sort(bitonic_test_array, n2);
	for (i = 0; i < n2; i++) {
		if (bitonic_test_array[i] != qarray[i]) {
			fprintf(stdout, "fail\n");
			failed++;
			pass = 0;
			break;
		}
	}
	if (pass) {
		fprintf(stdout, "pass\n");
		passed++;
	}
	free(bitonic_test_array);

	fprintf(stdout, "%d tests passed\n", passed);
	fprintf(stdout, "%d tests failed\n", failed);

	if (failed == 0) {
		return 0;
	}
	else {
		return 1;
	}
}
